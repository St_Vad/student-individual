import React, {useEffect, useState} from 'react';
import {Button, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";
import './../css/groups.css';
import {useDispatch, useSelector} from "react-redux";
import {useLocation} from "react-router-dom";
import {getIndividualTasks} from "../redux/thunks";
import CheckIcon from '@mui/icons-material/Check';
import {green} from "@mui/material/colors";
import CheckWorkForm from "./CheckWorkForm";

const Group = () => {
    const dispatch = useDispatch();
    const [openModal, setOpenModal] = useState(false);
    const [currentRow, setCurrentRow] = useState({});

    const search = useLocation().search;
    const groupId = new URLSearchParams(search).get('groupId');

    const workDisciplines = useSelector(state => state.workDisciplines);
    const individualTasksURL = useSelector(state => state.individualTasksURL);

    const students = workDisciplines.find(discipline => discipline.id === parseInt(groupId))?.workGroup?.students;

    const workDisciplineId = workDisciplines?.[0]?.id;

    const handleDownloadIndividualTasks = () => {
        dispatch(getIndividualTasks(workDisciplineId));
    }

    useEffect(() => {
        if (individualTasksURL) {
            const link = document.createElement('a');
            link.href = individualTasksURL;
            link.setAttribute('download', 'Индивидуальные задания.docx'); //or any other extension
            document.body.appendChild(link);
            link.click();
            dispatch({ type: 'DOWNLOAD_INDIVIDUAL_TASKS', url: '' });
        }
    }, [individualTasksURL]);

    const checkAllWorksCompleted = (work) => {
        return work?.tasks?.every((task) => task.complete);
    }

    const onClickCheckButton = (row) => () => {
        setCurrentRow(row.id);
        setOpenModal(true);
    }

    return (
        <>
            <h3 className="groups-title">Список студентов</h3>

            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell>ФИО</TableCell>
                            <TableCell>Группа</TableCell>
                            {
                                students?.[0].studentExaminationWorks?.map((work, index) =>
                                    <TableCell>КР{index + 1}</TableCell>)
                            }
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            students?.map((row) => (
                                <TableRow
                                    key={row?.student?.id}
                                    sx={{
                                        '&:last-child td, &:last-child th': { border: 0 },
                                        'cursor': 'pointer',
                                        '&:hover': {
                                            backgroundColor: '#eef4fc'
                                        }
                                    }}
                                    onClick={onClickCheckButton(row)}
                                >
                                    <TableCell component="th" scope="row">
                                        {row?.student?.fio}
                                    </TableCell>
                                    <TableCell>{row?.student?.group}</TableCell>
                                    {
                                        row?.studentExaminationWorks?.map(work => <TableCell align="center">
                                            {
                                                checkAllWorksCompleted(work) && <CheckIcon style={{ color: green[500] }} />
                                            }
                                        </TableCell>)
                                    }
                                </TableRow>
                            ))
                        }
                    </TableBody>
                </Table>
            </TableContainer>

            <div className="groups-buttons-wrapper">
                <Button
                    variant="outlined"
                    size="small"
                    sx={{ width: 220, marginRight: '20px' }}
                    onClick={handleDownloadIndividualTasks}
                >
                    Выгрузить работы
                </Button>

                {/*<Button*/}
                {/*    variant="outlined"*/}
                {/*    size="small"*/}
                {/*    sx={{ width: 220 }}*/}
                {/*>*/}
                {/*    Отметить успеваемость*/}
                {/*</Button>*/}
            </div>

            <CheckWorkForm
                openModal={openModal}
                setOpenModal={setOpenModal}
                currentRow={students?.find(item => item.id === currentRow)}
            />
        </>
    );
};

export default Group;
