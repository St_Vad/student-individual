import React from 'react';
import { Alert } from "@mui/material";

const SuccessAlert = ({ text, type }) => {
    return (
        <div style={{ display: "flex", justifyContent: "center" }}>
            <Alert severity={type} style={{ position: "absolute", top: 50 }}>{text}</Alert>
        </div>
    );
};

export default SuccessAlert;
