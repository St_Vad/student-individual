import React, { useState } from 'react';
import { Box, Checkbox, Modal, Typography } from "@mui/material";
import { setCompleteTask, setFailedTask } from "../redux/thunks";
import { useDispatch } from "react-redux";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 900,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const CheckWorkForm = ({ openModal, setOpenModal, currentRow }) => {
    const dispatch = useDispatch();

    console.log(currentRow)

    const onChangeCheckbox = (taskId) => (event) => {
        if (event.target.checked) {
            dispatch(setCompleteTask([taskId]));
        } else {
            dispatch(setFailedTask([taskId]));
        }
    }

    return (
        <Modal
            open={openModal}
            onClose={() => setOpenModal(false)}
        >
            <Box sx={style}>
                <Typography variant="h6" component="h2" sx={{ marginBottom: '20px' }}>
                    Отметить успеваемость
                </Typography>

                <Typography>Студент: {currentRow?.student?.fio}</Typography>

                {
                    currentRow?.studentExaminationWorks?.map((work) => (
                        <div style={{ margin: '15px 0' }}>
                            <Typography fontWeight="bold">Контрольная работа №{work.numberOfWork}</Typography>
                            {
                                work.tasks?.map((item, index) => (
                                    <Typography variant="body1" style={{ display: 'flex', alignItems: 'center' }}>
                                        {index + 1}. {item.task.text}
                                        <Checkbox
                                            checked={item.complete}
                                            onChange={onChangeCheckbox(item.id)}
                                        />
                                    </Typography>
                                ))
                            }
                        </div>
                    ))
                }
            </Box>
        </Modal>
    );
};

export default CheckWorkForm;
