import React, { useEffect } from 'react';
import { Route, Routes } from "react-router-dom";
import Discipline from "./Discipline";
import Group from "./Group";
import Registration from "./Registration";
import Login from "./Login";
import SuccessAlert from "./SuccessAlert";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router";

const Main = () => {
    const navigate = useNavigate();

    const { show, text, event } = useSelector(state => state.alert);
    const auth = useSelector(state => state.auth);

    useEffect(() => {
        const currentLocation = window.location.pathname;
        if (auth.isAuth && (currentLocation === '/registration' || currentLocation === '/login')) {
            navigate('/');
        }
        if (!auth.isAuth) {
            navigate('/login');
        }
    }, [auth.isAuth]);

    return (
        <>
            <Routes>
                <Route path="/" element={<Discipline />} />
                <Route path="/group" element={<Group />} />
                <Route path="/registration" element={<Registration />} />
                <Route path="/login" element={<Login />} />
            </Routes>

            {
                show && <SuccessAlert text={text} type={event} />
            }
        </>
    );
};

export default Main;
