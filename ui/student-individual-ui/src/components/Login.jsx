import React, { useEffect, useState } from 'react';
import { Button, Paper, TextField } from "@mui/material";
import './../css/registration.css';
import { useNavigate } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { handleLogin } from "../redux/thunks";

const Login = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const isAuth = useSelector(state => state.auth.isAuth);

    const [form, setForm] = useState({ login: '', password: '' });

    const onClickLoginButton = () => {
        dispatch(handleLogin(form));
    }

    const handleChange = (event) => {
        setForm((prevState) => ({
            ...prevState,
            [event.target.name]: event.target.value
        }))
    }

    useEffect(() => {
        if (isAuth) {
            navigate('/');
        }
    }, [isAuth, navigate]);

    const isValidForm = !!form.login && !!form.password;

    return (
        <Paper elevation={3} className="registration-form">
            <h3>Авторизация</h3>
            <TextField
                variant="outlined"
                name="login"
                label="Логин"
                fullWidth
                sx={{ marginBottom: '15px' }}
                value={form.login}
                onChange={handleChange}
            />
            <TextField
                variant="outlined"
                name="password"
                type="password"
                label="Пароль"
                fullWidth
                sx={{ marginBottom: '15px' }}
                value={form.password}
                onChange={handleChange}
            />
            <p className="has-account" onClick={() => navigate('/registration')}>Регистрация</p>

            <Button
                variant="contained"
                sx={{ margin: '15px 0' }}
                disabled={!isValidForm}
                onClick={onClickLoginButton}
            >
                Войти
            </Button>
        </Paper>
    );
};

export default Login;
