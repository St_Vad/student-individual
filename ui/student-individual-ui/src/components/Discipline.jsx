import React, { useEffect, useState } from 'react';
import {
    TableContainer,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Paper,
    Button,
} from "@mui/material";
import { useNavigate } from "react-router";
import './../css/discipline.css';
import {
    createWorkDisciplineThunk,
    getDisciplines,
    getGroups,
    getWorkDisciplines,
    handleUploadGroupFile,
    handleUploadSubjectFile
} from "../redux/thunks";
import { useDispatch, useSelector } from "react-redux";
import DisciplineModal from "./DisciplineModal";
import { options } from "axios";

const Discipline = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const auth = useSelector(state => state.auth);
    const workDisciplines = useSelector(state => state.workDisciplines);

    useEffect(() => {
        dispatch(getDisciplines(auth.teacher.id));
        dispatch(getGroups(auth.teacher.id));
        dispatch(getWorkDisciplines(auth.teacher.id));
    }, []);

    const [openModal, setOpenModal] = useState(false);

    const subjectFileName = useSelector((state) => state.subject.name);
    const groupFileName = useSelector((state) => state.group.name);

    const handleClickByRow = (discipline) => () => {
        navigate({
            pathname: '/group',
            search: `?groupId=${discipline.workGroup.id}`
        });
    }

    // xlsx
    const onUploadSubjectFile = (event) => {
        event?.target?.files && dispatch(handleUploadSubjectFile(event.target.files[0], auth.teacher.id));
    }

    // doc
    const onUploadGroupFile = (event) => {
        event?.target?.files && dispatch(handleUploadGroupFile(event.target.files[0], auth.teacher.id));
    }

    return (
        <>
            <h3 className="discipline-title">Рабочие дисциплины</h3>

            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell>Номер группы</TableCell>
                            <TableCell>Название предмета</TableCell>
                            <TableCell>Учебный поток</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            workDisciplines?.map((discipline) => (
                                <TableRow
                                    key={discipline.id}
                                    onClick={handleClickByRow(discipline)}
                                    sx={{
                                        '&:last-child td, &:last-child th': { border: 0 },
                                        'cursor': 'pointer',
                                        '&:hover': {
                                            backgroundColor: '#eef4fc'
                                        }
                                    }}
                                >
                                    <TableCell component="th" scope="row">
                                        {discipline.workGroup.name}
                                    </TableCell>
                                    <TableCell>{discipline.discipline.name}</TableCell>
                                    <TableCell>{discipline.workSemester}</TableCell>
                                </TableRow>
                            ))
                        }
                    </TableBody>
                </Table>
            </TableContainer>

            <div className="discipline-buttons-wrapper">
                <div className="discipline-upload-block">
                    <Button
                        variant="outlined"
                        component="label"
                        size="small"
                        sx={{ width: 220 }}
                    >
                        Добавить предмет (.xlsx)
                        <input type="file" hidden onChange={onUploadSubjectFile}/>
                    </Button>
                    {
                        !!subjectFileName && <span>Файл {subjectFileName} загружен!</span>
                    }
                </div>

                <div className="discipline-upload-block">
                    <Button
                        variant="outlined"
                        component="label"
                        size="small"
                        sx={{ width: 220 }}
                    >
                        Добавить группу (.doc)
                        <input type="file" hidden onChange={onUploadGroupFile}/>
                    </Button>
                    {
                        !!groupFileName && <span>Файл {groupFileName} загружен!</span>
                    }
                </div>

                <Button
                    variant="contained"
                    component="label"
                    size="small"
                    sx={{ marginTop: 20 + 'px', width: 220 }}
                    onClick={() => setOpenModal(true)}
                >
                    Добавить дисциплину
                </Button>
            </div>

            <DisciplineModal
                openModal={openModal}
                setOpenModal={setOpenModal}
            />
        </>
    );
};

export default Discipline;
