import axios from "axios";

const baseUrl = 'http://51.250.115.125:8091'

/** ПОЛУЧЕНИЕ ДИСЦИПЛИН */
export const getDisciplinesByTeacherId = async (teacherId) => {
    return await axios.get(`${baseUrl}/storchak/kubsu/discipline?teacherId=${teacherId}`);
}

export const getWorkDisciplinesByTeacher = async (teacherId) => {
    return await axios.get(`${baseUrl}/storchak/kubsu/work/discipline?teacherId=${teacherId}`);
}

/** ПОЛУЧЕНИЕ ГРУПП */
export const getWorkGroups = async (teacherId) => {
    return await axios.get(`${baseUrl}/storchak/kubsu/group?teacherId=${teacherId}`);
}

/** ЗАГРУЗКА ФАЙЛОВ */
export const uploadSubjectFile = async (data, teacherId) => {
    const response = await axios.post(`${baseUrl}/storchak/kubsu/individualTask/upload?teacherId=${teacherId}`, data);
    return response.status === 200;
}

export const uploadGroupFile = async (data, teacherId) => {
    const response = await axios.post(`${baseUrl}/storchak/kubsu/student/upload?teacherId=${teacherId}`, data);
    return response.status === 200;
}

/** РЕГИСТРАЦИЯ */
export const registration = async ({ name, secondName, login, password }) => {
    return await axios.post(`${baseUrl}/storchak/kubsu/teacher`, {
        name, secondName, login, password
    });
}


/** АВТОРИЗАЦИЯ */
export const authorization = async ({ login, passwordHash }) => {
    return await axios.post(`${baseUrl}/storchak/kubsu/teacher/login`, {
        login,
        password: passwordHash
    });
}

/** ВЫГРУЗИТЬ РАБОТЫ */
export const downloadIndividualWorks = async (workDisciplineId) => {
    return await axios.get(`${baseUrl}/storchak/kubsu/student/individual/works/report?workDisciplineId=${workDisciplineId}`, {
        responseType: 'blob'
    });
}

/** ОТМЕТКА ЗАДАНИЯ */
export const completeTask = async (taskIds) => {
    return await axios.post(`${baseUrl}/storchak/kubsu/student/mark/exam/complete`, {
        taskIds
    });
}

export const failedTask = async (taskIds) => {
    return await axios.post(`${baseUrl}/storchak/kubsu/student/mark/exam/failed`, {
        taskIds
    });
}

export const createWorkDiscipline = async ({ teacherId, disciplineId, groupId, workSemester }) => {
    return await axios.post(`${baseUrl}/storchak/kubsu/work/discipline`, {
        teacherId, disciplineId, groupId, workSemester
    })
}