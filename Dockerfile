#FROM openjdk17
# backend
# For ARM
FROM bellsoft/liberica-openjdk-alpine-musl:17
#ARG debug_port
ARG expose_port
ENV EXPOSE_PORT=8091
ENV DEBUG_PORT=8888
RUN echo Debug_port = $DEBUG_PORT, Expose_port = $EXPOSE_PORT
# указываем точку монтирования для внешних данных внутри контейнера
VOLUME /tmp

#какой порт контейнера будет открыт наружу
EXPOSE $EXPOSE_PORT

ADD target/student-individual-0.0.1-SNAPSHOT.jar app.jar
# команда запуска джарника
ENTRYPOINT exec java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=0.0.0.0:$DEBUG_PORT -jar /app.jar
