package kubsu.ru.studentindividual;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@SpringBootApplication
public class StudentIndividualApplication {
    public static void main(String[] args) {
        SpringApplication.run(StudentIndividualApplication.class, args);
    }

}
