package kubsu.ru.studentindividual.repository;

import kubsu.ru.studentindividual.entity.IndividualTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IndividualTaskRepository extends JpaRepository<IndividualTask, Long> {
}
