package kubsu.ru.studentindividual.repository;

import kubsu.ru.studentindividual.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long> {
    Teacher findTeacherByLogin(String login);
}
