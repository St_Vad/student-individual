package kubsu.ru.studentindividual.repository;

import kubsu.ru.studentindividual.entity.Theme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ThemeRepository extends JpaRepository<Theme, Long> {
    List<Theme> findThemeByDisciplineId(Long disciplineId);
}
