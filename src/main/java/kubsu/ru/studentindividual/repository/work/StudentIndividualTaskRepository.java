package kubsu.ru.studentindividual.repository.work;

import kubsu.ru.studentindividual.entity.work.StudentIndividualTask;
import kubsu.ru.studentindividual.entity.work.WorkDiscipline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentIndividualTaskRepository extends JpaRepository<StudentIndividualTask, Long> {
    @Modifying
    @Query("update student_individual_task task set task.isComplete = true where task.id = :id")
    void markTaskCompleted(@Param("id") Long id);
    @Modifying
    @Query("update student_individual_task task set task.isComplete = false where task.id in (:ids)")
    void markTasksFailed(@Param("ids") List<Long> ids);
}
