package kubsu.ru.studentindividual.repository.work;

import kubsu.ru.studentindividual.entity.work.WorkDiscipline;
import kubsu.ru.studentindividual.entity.work.WorkGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkGroupRepository extends JpaRepository<WorkGroup, Long> {
}
