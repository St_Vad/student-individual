package kubsu.ru.studentindividual.repository.work;

import kubsu.ru.studentindividual.entity.Discipline;
import kubsu.ru.studentindividual.entity.work.WorkDiscipline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkDisciplineRepository extends JpaRepository<WorkDiscipline, Long> {
    List<WorkDiscipline> findDisciplineByTeacherId(Long teacherId);
}
