package kubsu.ru.studentindividual.repository;

import kubsu.ru.studentindividual.entity.Competence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompetenceRepository extends JpaRepository<Competence, Long> {
    List<Competence> findCompetenceByDisciplineId(Long disciplineId);
}
