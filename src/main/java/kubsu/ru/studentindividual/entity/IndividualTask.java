package kubsu.ru.studentindividual.entity;

import jakarta.persistence.*;
import kubsu.ru.studentindividual.exception.EntityNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.poi.ss.usermodel.Row;

import java.util.List;

/**
 * Индивидуальное задание
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "individual_task")
public class IndividualTask {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "text", nullable = false, length = 2000)
    private String text;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "theme_id", referencedColumnName = "id")
    private Theme theme;
    @Column(name = "discipline_id", nullable = false)
    private Long disciplineId;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "task_competence",
            joinColumns = @JoinColumn(name = "task_id"),
            inverseJoinColumns = @JoinColumn(name = "competence_id"))
    private List<Competence> competence;

    @Column(name = "task_level", nullable = false)
    private TaskLevel taskLevel;

    public static IndividualTask fromRow(Row row, Discipline discipline, List<Competence> competences, List<Theme> themes) {
        String taskText = row.getCell(0).getStringCellValue();
        int themNumber = (int) row.getCell(1).getNumericCellValue();
        int level = (int) row.getCell(2).getNumericCellValue();
        List<String> competence = List.of(row.getCell(3).getStringCellValue().split(";"));
        return IndividualTask.builder()
                .text(taskText)
                .taskLevel(TaskLevel.ofNumber(level))
                .disciplineId(discipline.getId())
                .theme(themes.stream().filter(t -> t.getNumber() == themNumber).findFirst()
                        .orElseThrow(EntityNotFoundException.themeNotFound(themNumber)))
                .competence(competences.stream()
                        .filter(c -> competence.contains(c.getNumber().toString()))
                        .toList()
                )
                .build();
    }
}

