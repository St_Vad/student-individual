package kubsu.ru.studentindividual.entity;

import jakarta.persistence.*;
import lombok.*;
import org.apache.poi.ss.usermodel.Row;

@Data
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "theme")
public class Theme {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "text", nullable = false, length = 1000)
    private String text;

    @Column(name = "number", nullable = false)
    private Integer number;
    @Column(name = "examination_number", nullable = false)
    private Integer examinationNumber;

    @Column(name = "discipline_id", nullable = false)
    private Long disciplineId;

    public static Theme fromRow(Row row, Discipline discipline) {
        int themeNumber = (int) row.getCell(0).getNumericCellValue();
        String themeText = row.getCell(1).getStringCellValue();
        int examinationNumber = (int) row.getCell(2).getNumericCellValue();

        return Theme.builder()
                .number(themeNumber)
                .text(themeText)
                .disciplineId(discipline.getId())
                .examinationNumber(examinationNumber)
                .build();
    }
}
