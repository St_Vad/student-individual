package kubsu.ru.studentindividual.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.poi.ss.usermodel.Row;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "competence")
public class Competence {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "number", nullable = false)
    private Integer number;
    @Column(name = "text", nullable = false, length = 1000)
    private String text;

    @Column(name = "discipline_id", nullable = false)
    private Long disciplineId;

    public static Competence fromRow(Row row, Discipline discipline) {
        int competenceNumber = (int) row.getCell(0).getNumericCellValue();
        String competenceText = row.getCell(1).getStringCellValue();

        return Competence.builder()
                .number(competenceNumber)
                .text(competenceText)
                .disciplineId(discipline.getId())
                .build();
    }
}
