package kubsu.ru.studentindividual.entity;

import jakarta.persistence.*;
import kubsu.ru.studentindividual.exception.EntityNotFoundException;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import java.util.Comparator;
import java.util.List;

/**
 * Рабочая дисциплина
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@DynamicInsert
@Entity(name = "discipline")
public class Discipline {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * Название дисциплины
     */
    @Column(name = "name", nullable = false, length = 1000)
    private String name;
    /**
     * Преподаватель, ведущий дисциплину
     */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "teacher_id", referencedColumnName = "id")
    private Teacher teacher;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL, CascadeType.MERGE})
    @JoinTable(name = "discipline_tasks",
            joinColumns = @JoinColumn(name = "discipline_id"),
            inverseJoinColumns = @JoinColumn(name = "task_id"))
    private List<IndividualTask> tasks;

    public int countOfExamination() {
        return tasks.stream()
                .map(IndividualTask::getTheme)
                .map(Theme::getExaminationNumber)
                .max(Comparator.naturalOrder())
                .orElse(-1);
    }
}
