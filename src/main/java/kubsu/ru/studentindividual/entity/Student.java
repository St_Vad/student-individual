package kubsu.ru.studentindividual.entity;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Студент
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "fio", nullable = false)
    private String fio;
    @Column(name = "student_group", nullable = false)
    private String group;

    public static Student fromText(String text, String groupName) {
        return Student.builder()
                .fio(text)
                .group(groupName)
                .build();
    }
}
