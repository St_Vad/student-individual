package kubsu.ru.studentindividual.entity.work;

import jakarta.persistence.*;
import kubsu.ru.studentindividual.entity.IndividualTask;
import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "student_individual_task")
public class StudentIndividualTask {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "student_id", nullable = false)
    private Long studentId;

    /**
     * Индивидуальное задание студента
     */
    @OneToOne
    @JoinColumn(name = "task_id", referencedColumnName = "id")
    private IndividualTask task;
    /**
     * Статус выполнения работы студентом (зачтена или нет)
     */
    @Column(name = "is_complete")
    private boolean isComplete;
}
