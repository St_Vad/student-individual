package kubsu.ru.studentindividual.entity.work;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "student_examination_work")
public class StudentExaminationWork {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /**
     * Номер контрольной работы
     */
    @Column(name = "number_of_work", nullable = false)
    private int numberOfWork;

    @Column(name = "student_id", nullable = false)
    private Long studentId;

    /**
     * Список индивидуальных задач в рамках контрольной работы
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL, CascadeType.MERGE})
    @JoinTable(name = "exam_tasks",
            joinColumns = @JoinColumn(name = "exam_id"),
            inverseJoinColumns = @JoinColumn(name = "task_id"))
    private List<StudentIndividualTask> tasks;
}
