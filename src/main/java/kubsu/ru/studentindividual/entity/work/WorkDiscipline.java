package kubsu.ru.studentindividual.entity.work;

import jakarta.persistence.*;
import kubsu.ru.studentindividual.entity.Discipline;
import kubsu.ru.studentindividual.entity.Group;
import kubsu.ru.studentindividual.entity.Teacher;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "work_discipline")
public class WorkDiscipline {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /**
     * Идентификатор преподователя, ведущего дисциплину
     */
    @OneToOne
    @JoinColumn(name = "teacher_id", referencedColumnName = "id")
    private Teacher teacher;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "work_group_id", referencedColumnName = "id")
    private WorkGroup workGroup;

    @OneToOne
    @JoinColumn(name = "discipline_id", referencedColumnName = "id")
    private Discipline discipline;

    /**
     * Рабочий семестр
     * ex. 22/23, 23/24
     */
    @Column(name = "work_semester", nullable = false)
    private String workSemester;
}
