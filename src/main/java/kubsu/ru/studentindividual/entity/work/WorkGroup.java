package kubsu.ru.studentindividual.entity.work;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "work_groups")
public class WorkGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", nullable = false, length = 1000)
    private String name;

    /**
     * Идентификатор преподавателя, создавшего группу
     */
    @Column(name = "teacher_id", nullable = false)
    private Long teacherId;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL, CascadeType.MERGE})
    @JoinTable(name = "work_group_students",
            joinColumns = @JoinColumn(name = "group_id"),
            inverseJoinColumns = @JoinColumn(name = "student_id"))
    private List<WorkStudent> students;
}
