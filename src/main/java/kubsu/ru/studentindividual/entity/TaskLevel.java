package kubsu.ru.studentindividual.entity;

public enum TaskLevel {
    easy,
    medium,
    high;

    public static TaskLevel ofNumber(int number) {
        return switch (number) {
            case 1 -> easy;
            case 3 -> high;
            default -> medium;
        };
    }
}
