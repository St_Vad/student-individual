package kubsu.ru.studentindividual.exception;

import java.util.function.Supplier;

public class EntityNotFoundException extends RuntimeException implements Supplier<EntityNotFoundException> {

    private EntityNotFoundException(String message) {
        super(message);
    }

    public static EntityNotFoundException studentNotFound(Long studentId) {
        return new EntityNotFoundException("Student with id = " + studentId + "not found");
    }

    public static EntityNotFoundException teacherNotFound(Long teacherId) {
        return new EntityNotFoundException("Teacher with id = " + teacherId + "not found");
    }

    public static EntityNotFoundException lessonNotFound(Long lessonId) {
        return new EntityNotFoundException("Lesson with id = " + lessonId + "not found");
    }

    public static EntityNotFoundException themeNotFound(int themNumber) {
        return new EntityNotFoundException("Theme with number = " + themNumber + "not found");
    }

    public static EntityNotFoundException objectNotFound(String object, Long entityId) {
        return new EntityNotFoundException(object + "with id = " + entityId + "not found");
    }
    public static EntityNotFoundException objectNotFound(String text) {
        return new EntityNotFoundException(text);
    }

    /**
     * Gets a result.
     *
     * @return a result
     */
    @Override
    public EntityNotFoundException get() {
        return this;
    }
}
