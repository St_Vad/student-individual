package kubsu.ru.studentindividual.exception;

import java.util.function.Supplier;

public class FailedLoginUserException extends RuntimeException implements Supplier<FailedLoginUserException> {
    private FailedLoginUserException(String message) {
        super(message);
    }

    public static FailedLoginUserException studentFailedRegister() {
        return new FailedLoginUserException("Student failed register");
    }
    public static FailedLoginUserException teacherFailedRegister() {
        return new FailedLoginUserException("Teacher failed register");
    }

    /**
     * Gets a result.
     *
     * @return a result
     */
    @Override
    public FailedLoginUserException get() {
        return this;
    }
}
