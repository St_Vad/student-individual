package kubsu.ru.studentindividual.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import kubsu.ru.studentindividual.controller.dto.CalculateCurrencyResponse;
import kubsu.ru.studentindividual.controller.dto.CalculateRequest;
import kubsu.ru.studentindividual.controller.dto.CurrencyResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.*;

@Slf4j
@CrossOrigin("*")
@RestController
@AllArgsConstructor
@RequestMapping(path = "/shubin/money", produces = "application/json")
public class MoneyConverterController {
    private final RestTemplate restTemplate = new RestTemplate();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final Map<String, Double> keyByNominalCacheMap = new HashMap<>();

    @GetMapping("/currency")
    public List<CurrencyResponse> getCurrency() throws IOException {
        Map<String, Object> valutes = getValutes();
        return valutes.values().stream().map(valuteMeta -> {
            Map<String, String> valuteMetaMap = (Map<String, String>) valuteMeta;
            return CurrencyResponse.builder()
                    .key(valuteMetaMap.get("CharCode"))
                    .name(valuteMetaMap.get("Name"))
                    .build();
        }).toList();
    }

    private Map<String, Object> getValutes() {
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
        messageConverters.add(converter);
        restTemplate.setMessageConverters(messageConverters);
        Map<String, Object> forObject = restTemplate.getForObject("https://www.cbr-xml-daily.ru/daily_json.js", Map.class);
        Map<String, Object> valutes = (Map<String, Object>) forObject.get("Valute");
        valutes.values().forEach(valute -> {
            Map<String, Object> valuteMetaMap = (Map<String, Object>) valute;
            String key = String.valueOf(valuteMetaMap.get("CharCode"));
            Double value = (Double) valuteMetaMap.get("Value");
            keyByNominalCacheMap.put(key, value);
        });
        return valutes;
    }

    @PostMapping("/currency")
    public CalculateCurrencyResponse calculateCurrency(@RequestBody CalculateRequest request) throws IOException {
        Double currency = keyByNominalCacheMap.get(request.getKey());
        if (currency == null) {
            getValutes();
            currency = keyByNominalCacheMap.get(request.getKey());
        }
        if (currency == null) {
            throw new RuntimeException("Такая валюта не найдена");
        }
        return CalculateCurrencyResponse.builder()
                .value(request.getValue() != null ? request.getValue() * currency : 0D)
                .valute("RUB")
                .build();
    }
}
