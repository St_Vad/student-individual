package kubsu.ru.studentindividual.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import kubsu.ru.studentindividual.entity.Group;
import kubsu.ru.studentindividual.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequiredArgsConstructor
@RequestMapping(path = "/storchak/kubsu/group", produces = "application/json")
@Tag(name = "My API", description = "My API")
public class GroupController {
    private final GroupService groupService;

    @GetMapping
    public List<Group> getGroupByTeacher(@RequestParam Long teacherId) {
        return groupService.findByTeacherId(teacherId);
    }
}
