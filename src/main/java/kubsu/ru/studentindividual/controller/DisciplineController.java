package kubsu.ru.studentindividual.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import kubsu.ru.studentindividual.entity.Discipline;
import kubsu.ru.studentindividual.service.DisciplineService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequiredArgsConstructor
@RequestMapping(path = "/storchak/kubsu/discipline", produces = "application/json")
@Tag(name = "My API", description = "My API")
public class DisciplineController {
    private final DisciplineService disciplineService;
    @GetMapping
    public List<Discipline> getDisciplineByTeacher(@RequestParam Long teacherId) {
        return disciplineService.findByTeacherId(teacherId);
    }
}
