package kubsu.ru.studentindividual.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import kubsu.ru.studentindividual.controller.dto.request.MarkExamRequest;
import kubsu.ru.studentindividual.entity.Group;
import kubsu.ru.studentindividual.service.ReportService;
import kubsu.ru.studentindividual.service.StudentService;
import kubsu.ru.studentindividual.service.dto.WorkGroupReportView;
import kubsu.ru.studentindividual.service.generators.DocxGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ContentDisposition;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequiredArgsConstructor
@RequestMapping(path = "/storchak/kubsu/student", produces = "application/json")
@Tag(name = "My API", description = "My API")
public class StudentController {
    private final StudentService studentService;
    private final ReportService reportService;

    @PostMapping("upload")
    public List<Group> addStudents(@RequestParam("file") MultipartFile file, @RequestParam Long teacherId) throws IOException {
        return studentService.uploadStudents(file, teacherId);
    }

    @PostMapping("/mark/exam/complete")
    public void markExamComplete(@RequestBody MarkExamRequest request) {
        studentService.markExamComplete(request);
    }

    @PostMapping("/mark/exam/failed")
    public void markExamFailed(@RequestBody MarkExamRequest request) {
        studentService.markExamFailed(request);
    }

    @GetMapping("/individual/works/report")
    public void generateReport(HttpServletResponse response, @RequestParam Long workDisciplineId) throws IOException {
        WorkGroupReportView view = reportService.createReportForWorkGroup(workDisciplineId);

        ServletOutputStream outputStream = response.getOutputStream();
        view.getStream().transferTo(outputStream);
        ContentDisposition contentDisposition = ContentDisposition.builder("attachment")
                .filename(view.getFileName())
                .build();
        response.setHeader("Content-Disposition", contentDisposition.toString());
        response.flushBuffer();
        DocxGenerator.removeTmpFile(view.getFileName());
    }
}
