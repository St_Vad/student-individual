package kubsu.ru.studentindividual.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import kubsu.ru.studentindividual.entity.IndividualTask;
import kubsu.ru.studentindividual.exception.FailedParseException;
import kubsu.ru.studentindividual.service.IndividualTaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@CrossOrigin("*")
@RequiredArgsConstructor
@RequestMapping(path = "/storchak/kubsu/individualTask", produces = "application/json")
@Tag(name = "My API", description = "My API")
public class IndividualTaskController {
    private final IndividualTaskService individualTaskService;

    @PostMapping
    public IndividualTask addIndividualTask(@RequestBody IndividualTask individualTask) {
        return individualTaskService.addIndividualTask(individualTask);
    }

    @PostMapping("upload")
    public void addIndividualTask(@RequestParam("file") MultipartFile file, @RequestParam Long teacherId) throws IOException, FailedParseException {
        individualTaskService.parseTasks(file, teacherId);
    }
}
