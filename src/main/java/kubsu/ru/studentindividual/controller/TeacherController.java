package kubsu.ru.studentindividual.controller;


import io.swagger.v3.oas.annotations.tags.Tag;
import kubsu.ru.studentindividual.controller.dto.request.LoginTeacherRequest;
import kubsu.ru.studentindividual.controller.dto.response.LoginTeacherResponse;
import kubsu.ru.studentindividual.controller.dto.response.TeacherCreateResponse;
import kubsu.ru.studentindividual.entity.Teacher;
import kubsu.ru.studentindividual.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequiredArgsConstructor
@RequestMapping(path = "/storchak/kubsu/teacher", produces = "application/json")
@Tag(name = "My API", description = "My API")
public class TeacherController {
    private final TeacherService teacherService;

    @PostMapping
    public TeacherCreateResponse addTeacher(@RequestBody Teacher teacher) {
        return TeacherCreateResponse.fromTeacher(teacherService.addTeacher(teacher));
    }

    @PostMapping("/login")
    public LoginTeacherResponse loginTeacher(@RequestBody LoginTeacherRequest request) {
        return teacherService.login(request);
    }

}
