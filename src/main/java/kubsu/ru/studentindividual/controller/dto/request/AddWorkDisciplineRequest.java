package kubsu.ru.studentindividual.controller.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddWorkDisciplineRequest {
    private Long teacherId;
    private Long disciplineId;
    private Long groupId;
    private String workSemester;
}
