package kubsu.ru.studentindividual.controller.dto.response;

import kubsu.ru.studentindividual.entity.Teacher;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginTeacherResponse {
    private Long id;
    private String name;
    private String secondName;
    private String login;
    private String message;

    public static LoginTeacherResponse fromTeacher(Teacher teacher) {
        return LoginTeacherResponse.builder()
                .id(teacher.getId())
                .name(teacher.getName())
                .secondName(teacher.getSecondName())
                .login(teacher.getLogin())
                .message("Register Success")
                .build();
    }
}
