package kubsu.ru.studentindividual.controller.dto.response;

import kubsu.ru.studentindividual.entity.Teacher;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TeacherCreateResponse {
    private Long id;
    private String name;
    private String secondName;
    private String login;

    public static TeacherCreateResponse fromTeacher(Teacher teacher) {
        return TeacherCreateResponse.builder()
                .id(teacher.getId())
                .name(teacher.getName())
                .secondName(teacher.getSecondName())
                .login(teacher.getLogin())
                .build();
    }
}
