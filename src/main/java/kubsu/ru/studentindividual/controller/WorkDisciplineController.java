package kubsu.ru.studentindividual.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import kubsu.ru.studentindividual.controller.dto.request.AddWorkDisciplineRequest;
import kubsu.ru.studentindividual.entity.work.WorkDiscipline;
import kubsu.ru.studentindividual.service.WorkDisciplineService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequiredArgsConstructor
@RequestMapping(path = "/storchak/kubsu/work/discipline", produces = "application/json")
@Tag(name = "My API", description = "My API")
public class WorkDisciplineController {
    private final WorkDisciplineService disciplineService;

    @GetMapping
    public List<WorkDiscipline> getDisciplineByTeacher(@RequestParam Long teacherId) {
        return disciplineService.findByTeacherId(teacherId);
    }

    @PostMapping
    public WorkDiscipline addWorkDiscipline(@RequestBody AddWorkDisciplineRequest request) {
        return disciplineService.addDiscipline(request);
    }
}
