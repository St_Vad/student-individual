package kubsu.ru.studentindividual.service;

import kubsu.ru.studentindividual.controller.dto.request.AddWorkDisciplineRequest;
import kubsu.ru.studentindividual.entity.*;
import kubsu.ru.studentindividual.entity.work.*;
import kubsu.ru.studentindividual.exception.EntityNotFoundException;
import kubsu.ru.studentindividual.repository.*;
import kubsu.ru.studentindividual.repository.work.WorkDisciplineRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class WorkDisciplineService {
    private final WorkDisciplineRepository workDisciplineRepository;
    private final DisciplineRepository disciplineRepository;
    private final TeacherRepository teacherRepository;
    private final ThemeRepository themeRepository;
    private final CompetenceRepository competenceRepository;
    private final GroupRepository groupRepository;

    public List<WorkDiscipline> findByTeacherId(Long teacherId) {
        return workDisciplineRepository.findDisciplineByTeacherId(teacherId).stream().peek(discipline -> {
            discipline.getDiscipline().setTasks(null);
            discipline.getDiscipline().setTeacher(null);
            discipline.getWorkGroup().getStudents()
                    .forEach(student -> student.getStudentExaminationWorks()
                            .forEach(work -> work.setTasks(work.getTasks().stream()
                                    .sorted(Comparator.comparing(StudentIndividualTask::getId))
                                    .toList()))
                    );
        }).toList();
    }

    public WorkDiscipline addDiscipline(AddWorkDisciplineRequest request) {
        Discipline discipline = disciplineRepository.findById(request.getDisciplineId())
                .orElseThrow(EntityNotFoundException.objectNotFound("Discipline", request.getDisciplineId()));
        Group group = groupRepository.findById(request.getGroupId())
                .orElseThrow(EntityNotFoundException.objectNotFound("Group", request.getGroupId()));
        Teacher teacher = teacherRepository.findById(request.getTeacherId())
                .orElseThrow(EntityNotFoundException.objectNotFound("Teacher", request.getTeacherId()));
        List<Theme> themes = themeRepository.findThemeByDisciplineId(discipline.getId());
        if (CollectionUtils.isEmpty(themes)) {
            throw EntityNotFoundException.objectNotFound("Theme for Discipline", discipline.getId());
        }
        int countOfExams = discipline.countOfExamination();
        WorkGroup workGroup = WorkGroup.builder()
                .teacherId(teacher.getId())
                .name(group.getName())
                .students(group.getStudents().stream().map(student -> {
                            List<StudentExaminationWork> examinationWorks = new ArrayList<>(countOfExams);
                            List<Competence> competences = competenceRepository.findCompetenceByDisciplineId(discipline.getId());
                            for (int i = 1; i <= countOfExams; i++) {
                                List<Theme> examThemes = getThemesByExamNumber(themes, i);
                                examinationWorks.add(StudentExaminationWork.builder()
                                        .numberOfWork(i)
                                        .studentId(student.getId())
                                        .tasks(getTasksByEachTheme(discipline.getTasks(), examThemes, student.getId(), competences))
                                        .build());
                            }
                            return WorkStudent.builder()
                                    .student(student)
                                    .studentExaminationWorks(examinationWorks)
                                    .build();
                        }).toList()
                )
                .build();

        WorkDiscipline workDiscipline = WorkDiscipline.builder()
                .discipline(discipline)
                .workGroup(workGroup)
                .workSemester(request.getWorkSemester())
                .teacher(teacher)
                .build();
        workDisciplineRepository.save(workDiscipline);
        workDiscipline.getDiscipline().setTasks(null);
        workDiscipline.getDiscipline().setTeacher(null);
        return workDiscipline;
    }

    private List<Theme> getThemesByExamNumber(List<Theme> themes, int examNumber) {
        return themes.stream()
                .filter(theme -> examNumber == theme.getExaminationNumber())
                .toList();
    }

    public List<StudentIndividualTask> getTasksByEachTheme(List<IndividualTask> tasks, List<Theme> themes, Long studentId, List<Competence> competences) {
        return themes.stream().map(theme -> {
                            List<IndividualTask> tasksByTheme = tasks.stream()
                                    .filter(task -> theme.equals(task.getTheme()) && !CollectionUtils.intersection(task.getCompetence(), competences).isEmpty())
                                    .sorted((o1, o2) -> {
                                        Collection<Competence> competences1 = CollectionUtils.intersection(o1.getCompetence(), competences);
                                        Collection<Competence> competences2 = CollectionUtils.intersection(o2.getCompetence(), competences);
                                        return competences2.size() - competences1.size();
                                    })
                                    .toList();
                            IndividualTask resultTask;
                            if (CollectionUtils.isNotEmpty(tasksByTheme)) {
                                Random random = new Random();
                                resultTask = tasksByTheme.get(random.nextInt(tasksByTheme.size() % 10));
                            } else {
                                resultTask = tasks.stream()
                                        .filter(task -> theme.equals(task.getTheme()))
                                        .findAny()
                                        .orElseThrow(EntityNotFoundException.objectNotFound("Task for theme not found"));
                            }
                            competences.removeAll(resultTask.getCompetence());
                            return StudentIndividualTask.builder()
                                    .studentId(studentId)
                                    .isComplete(false)
                                    .task(resultTask)
                                    .build();
                        }
                )
                .toList();

    }
}
