package kubsu.ru.studentindividual.service.utils;

import com.google.common.hash.Hashing;

import java.nio.charset.Charset;

public class HashingUtils {
    public static String hashPassword(String password) {
        return Hashing.sha512().hashString(password, Charset.defaultCharset()).toString();
    }
}
