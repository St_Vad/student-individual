package kubsu.ru.studentindividual.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.InputStream;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkGroupReportView {
    private String fileName;
    private String fileSize;
    private InputStream stream;
}
