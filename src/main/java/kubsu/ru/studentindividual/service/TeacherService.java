package kubsu.ru.studentindividual.service;

import kubsu.ru.studentindividual.controller.dto.request.LoginTeacherRequest;
import kubsu.ru.studentindividual.controller.dto.response.LoginTeacherResponse;
import kubsu.ru.studentindividual.entity.Teacher;
import kubsu.ru.studentindividual.exception.FailedLoginUserException;
import kubsu.ru.studentindividual.repository.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TeacherService {
    private final TeacherRepository teacherRepository;
    public Teacher addTeacher(Teacher teacher) {
        teacher.hashPassword();
        return teacherRepository.save(teacher);
    }

    public LoginTeacherResponse login(LoginTeacherRequest request) {
        Teacher teacher = teacherRepository.findTeacherByLogin(request.getLogin());
        if (teacher != null && teacher.getPassword().equals(request.getPassword())) {
            return LoginTeacherResponse.fromTeacher(teacher);
        }
        throw FailedLoginUserException.teacherFailedRegister();
    }
}
