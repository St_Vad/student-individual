package kubsu.ru.studentindividual.service.generators;

import kubsu.ru.studentindividual.entity.work.WorkDiscipline;
import kubsu.ru.studentindividual.service.dto.WorkGroupReportView;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Component
public class DocxGenerator {

    @EventListener(ApplicationStartedEvent.class)
    public void onStart() {
        File file = new File("./tmp/");
        file.mkdirs();
    }

    public static WorkGroupReportView generateReport(WorkDiscipline workDiscipline) throws IOException {
        File file = new File("./tmp/" + getFileName(workDiscipline));
        file.createNewFile();
        XWPFDocument document = new XWPFDocument();
        workDiscipline.getWorkGroup().getStudents().forEach(student -> {
            XWPFParagraph paragraph = document.createParagraph();
            XWPFRun runStudentName = paragraph.createRun();
            runStudentName.setText(student.getStudent().getFio() + "\t" + student.getStudent().getGroup());
            runStudentName.setBold(true);
            runStudentName.setCapitalized(true);
            student.getStudentExaminationWorks().forEach(exam -> {
                XWPFRun runHeader = document.createParagraph().createRun();
                runHeader.setText("Контрольная работа " + exam.getNumberOfWork());
                runHeader.setBold(true);
                AtomicInteger counter = new AtomicInteger(1);
                exam.getTasks().forEach(task -> {
                    XWPFRun runTask = document.createParagraph().createRun();
                    runTask.setText(counter.getAndIncrement() + "\t" + task.getTask().getText());
                });
            });
            paragraph.setPageBreak(true);
        });

        document.write(new FileOutputStream(file));
        document.close();

        return WorkGroupReportView.builder()
                .fileName(getFileName(workDiscipline))
                .stream(new FileInputStream(file))
                .build();
    }

    public static void removeTmpFile(String fileName) {
        File file = new File("./tmp/" + fileName);
        file.delete();
    }

    private static String getFileName(WorkDiscipline workDiscipline) {
        return workDiscipline.getWorkGroup().getName() + "_задачи.docx";
    }
}
