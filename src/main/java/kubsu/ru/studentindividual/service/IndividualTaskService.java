package kubsu.ru.studentindividual.service;

import kubsu.ru.studentindividual.entity.Discipline;
import kubsu.ru.studentindividual.entity.IndividualTask;
import kubsu.ru.studentindividual.exception.EntityNotFoundException;
import kubsu.ru.studentindividual.exception.FailedParseException;
import kubsu.ru.studentindividual.repository.DisciplineRepository;
import kubsu.ru.studentindividual.repository.IndividualTaskRepository;
import kubsu.ru.studentindividual.repository.TeacherRepository;
import kubsu.ru.studentindividual.service.parsers.XlsxParser;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class IndividualTaskService {
    private final IndividualTaskRepository taskRepository;
    private final DisciplineRepository disciplineRepository;
    private final TeacherRepository teacherRepository;
    private final XlsxParser xlsxParser;

    public IndividualTask addIndividualTask(IndividualTask individualTask) {
        return taskRepository.save(individualTask);
    }

    public Discipline parseTasks(MultipartFile file, Long teacherId) throws IOException, FailedParseException {
        Discipline result = xlsxParser.parseExcelDocument(file.getInputStream(), teacherRepository.findById(teacherId)
                .orElseThrow(EntityNotFoundException.teacherNotFound(teacherId)), file.getOriginalFilename());
        return result;
    }
}
