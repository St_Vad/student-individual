package kubsu.ru.studentindividual.service.parsers;

import kubsu.ru.studentindividual.entity.*;
import kubsu.ru.studentindividual.exception.FailedParseException;
import kubsu.ru.studentindividual.repository.DisciplineRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class XlsxParser {
    private final DisciplineRepository disciplineRepository;

    public Discipline parseExcelDocument(InputStream inputStream, Teacher teacher, String fileName) throws IOException, FailedParseException {
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        Discipline discipline = new Discipline();
        discipline.setTeacher(teacher);
        discipline.setName(fileName);
        discipline.setTasks(new ArrayList<>());
        disciplineRepository.saveAndFlush(discipline);
        List<Theme> themes = new ArrayList<>(extractThemes(workbook, discipline));
        List<Competence> competences = new ArrayList<>(extractCompetences(workbook, discipline));
        List<IndividualTask> tasks = new ArrayList<>(extractTasks(workbook, discipline, competences, themes));
        discipline.setTasks(tasks);
        disciplineRepository.saveAndFlush(discipline);
        return discipline;
    }

    private List<IndividualTask> extractTasks(XSSFWorkbook workbook, Discipline discipline, List<Competence> competences, List<Theme> themes) throws FailedParseException {
        Sheet sheet = workbook.getSheet("Контрольные Задачи");
        if (sheet == null) {
            throw new FailedParseException();
        }
        List<IndividualTask> result = new ArrayList<>();
        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
            Row row = sheet.getRow(i);
            try {
                result.add(IndividualTask.fromRow(row, discipline, competences, themes));
            } catch (NullPointerException ignored) {
            }
        }
        return result;
    }

    private List<Competence> extractCompetences(XSSFWorkbook workbook, Discipline discipline) throws FailedParseException {
        Sheet sheet = workbook.getSheet("ПК");
        if (sheet == null) {
            throw new FailedParseException();
        }
        List<Competence> result = new ArrayList<>();
        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
            Row row = sheet.getRow(i);
            try {
                result.add(Competence.fromRow(row, discipline));
            } catch (NullPointerException ignored) {
            }
        }
        return result;
    }

    private List<Theme> extractThemes(XSSFWorkbook workbook, Discipline discipline) throws FailedParseException {
        Sheet sheet = workbook.getSheet("Темы");
        if (sheet == null) {
            throw new FailedParseException();
        }
        List<Theme> result = new ArrayList<>();
        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
            Row row = sheet.getRow(i);
            result.add(Theme.fromRow(row, discipline));
        }
        return result;
    }
}
