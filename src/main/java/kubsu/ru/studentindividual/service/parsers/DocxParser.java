package kubsu.ru.studentindividual.service.parsers;

import kubsu.ru.studentindividual.entity.Group;
import kubsu.ru.studentindividual.entity.Student;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


@Slf4j
public class DocxParser {

    public static List<Group> parseStudentsDocument(InputStream inputStream, Long teacherId) throws IOException {
        XWPFDocument docx = new XWPFDocument(inputStream);
        List<Group> result = new ArrayList<>();
        Group currGroup = null;
        for (XWPFParagraph paragraph : docx.getParagraphs()) {
            String text = paragraph.getParagraphText();
            if (text.contains("группа")) {
                currGroup = new Group();
                currGroup.setName(text);
                currGroup.setStudents(new ArrayList<>());
                currGroup.setTeacherId(teacherId);
                continue;
            }
            if (StringUtils.isNotBlank(text) && currGroup != null) {
                currGroup.getStudents().add(Student.fromText(text, currGroup.getName()));
            }
            if (StringUtils.isBlank(text) && currGroup != null && CollectionUtils.isNotEmpty(currGroup.getStudents())) {
                result.add(currGroup);
                currGroup = null;
            }
        }
        docx.close();
        return result;
    }
}
