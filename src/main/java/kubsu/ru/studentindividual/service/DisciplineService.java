package kubsu.ru.studentindividual.service;

import kubsu.ru.studentindividual.entity.Discipline;
import kubsu.ru.studentindividual.repository.DisciplineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DisciplineService {
    private final DisciplineRepository disciplineRepository;
    public List<Discipline> findByTeacherId(Long teacherId) {
        return disciplineRepository.findDisciplineByTeacherId(teacherId);
    }
}
