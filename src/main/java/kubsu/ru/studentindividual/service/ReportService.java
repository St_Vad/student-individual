package kubsu.ru.studentindividual.service;

import kubsu.ru.studentindividual.entity.work.WorkDiscipline;
import kubsu.ru.studentindividual.exception.EntityNotFoundException;
import kubsu.ru.studentindividual.repository.work.WorkDisciplineRepository;
import kubsu.ru.studentindividual.service.dto.WorkGroupReportView;
import kubsu.ru.studentindividual.service.generators.DocxGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class ReportService {
    private final WorkDisciplineRepository workDisciplineRepository;

    public WorkGroupReportView createReportForWorkGroup(Long workGroupId) throws IOException {
        WorkDiscipline workDiscipline = workDisciplineRepository.findById(workGroupId)
                .orElseThrow(EntityNotFoundException.objectNotFound("WorkDiscipline", workGroupId));
        WorkGroupReportView result = DocxGenerator.generateReport(workDiscipline);
        return result;
    }
}
