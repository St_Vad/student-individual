package kubsu.ru.studentindividual.service;

import kubsu.ru.studentindividual.entity.Group;
import kubsu.ru.studentindividual.repository.GroupRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GroupService {
    private final GroupRepository groupRepository;
    public List<Group> findByTeacherId(Long teacherId) {
        return groupRepository.findGroupByTeacherId(teacherId);
    }
}
