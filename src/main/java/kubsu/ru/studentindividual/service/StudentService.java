package kubsu.ru.studentindividual.service;

import kubsu.ru.studentindividual.controller.dto.request.MarkExamRequest;
import kubsu.ru.studentindividual.entity.Group;
import kubsu.ru.studentindividual.entity.work.StudentIndividualTask;
import kubsu.ru.studentindividual.entity.work.WorkGroup;
import kubsu.ru.studentindividual.repository.GroupRepository;
import kubsu.ru.studentindividual.repository.StudentRepository;
import kubsu.ru.studentindividual.repository.work.StudentIndividualTaskRepository;
import kubsu.ru.studentindividual.service.parsers.DocxParser;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;
    private final StudentIndividualTaskRepository studentIndividualTaskRepository;
    private final GroupRepository groupRepository;
    public List<Group> uploadStudents(MultipartFile file, Long teacherId) throws IOException {
        List<Group> result = DocxParser.parseStudentsDocument(file.getInputStream(), teacherId);

        return groupRepository.saveAllAndFlush(result);
    }

    public void markExamComplete(MarkExamRequest request) {
        List<StudentIndividualTask> tasks =  studentIndividualTaskRepository.findAllById(request.getTaskIds());
        tasks.forEach(task -> task.setComplete(true));
        studentIndividualTaskRepository.saveAll(tasks);
    }

    public void markExamFailed(MarkExamRequest request) {
        List<StudentIndividualTask> tasks =  studentIndividualTaskRepository.findAllById(request.getTaskIds());
        tasks.forEach(task -> task.setComplete(false));
        studentIndividualTaskRepository.saveAll(tasks);
    }
}
